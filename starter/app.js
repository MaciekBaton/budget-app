var budgetController = (function() {
    var Expense = function(id, description, value, percentage){
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = percentage;
    };
    var Income = function(id, description, value){
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        percentage:{
            exp: 0,
            inc: 0
        }
    }

    return {
        addItem: function(type, des, val){

            var newItem, ID, percentage;
            if (data.allItems[type].length > 0){
            ID = data.allItems[type][data.allItems[type].length -1].id +1;
            } else {
                ID = 0;
            }
           
            if (type === 'inc'){
                newItem = new Income(ID, des, val);
            } else if (type === 'exp'){
                newItem = new Expense(ID, des, val, percentage);
            }
            data.allItems[type].push(newItem);
            return newItem;
        },
        deleteItem: function(type, id){
            var ids, index;
            ids = data.allItems[type].map(function(curr){
                return curr.id;
            });
            index = ids.indexOf(id);
            
            if (index !== -1){
                data.allItems[type].splice(index, 1);
            }
        },
        calculateTotals: function(){
            data.totals['inc'] = 0;
            data.totals['exp'] = 0;
            data.allItems['inc'].forEach(function(element){
                data.totals['inc'] += parseInt(element.value);
                
            });

            data.allItems['exp'].forEach(function(element){
                data.totals['exp'] += parseInt(element.value);
                
            });
            
        },
        calculatePercentages: function(){
            data.percentage['inc'] = Math.round(data.totals['inc']/(data.totals['exp'] + data.totals['inc'])*100);
            data.percentage['exp'] = Math.round(data.totals['exp']/(data.totals['exp'] + data.totals['inc'])*100);
            data.allItems['exp'].forEach(function(element){
                element.percentage = Math.round(element.value/(data.totals['exp'] + data.totals['inc'])*100);
            });
        },
        getData: function(){
            return data;
        },
        saveData: function(){
            if (localStorage.getItem('DBs') === null || localStorage.getItem('DBs1') === null || localStorage.getItem('DBs2') === null){
                DBs = [];
            } else{
            var DBs = JSON.parse(localStorage.getItem('DBs'));

            }
            localStorage.clear();
            DBs.push(data);

            localStorage.setItem('DBs', JSON.stringify(DBs));

        },
        readData: function(){
            var tmp = localStorage.getItem('DBs');
            var GBs = JSON.parse(tmp);
            data = GBs[0];
        }
    };
})();

var UIcontroller = (function() {
   DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputButton: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        totalInc: '.budget__income--value',
        totalExp: '.budget__expenses--value',
        totalBudget: '.budget__value',
        UIMonth: '.budget',
        expPercentage: '.budget__expenses--percentage',
        incPercentage: '.budget__income--percentage',
        container: '.container'

    };

    return{
        getinput: function(){
            return{
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: document.querySelector(DOMstrings.inputValue).value
            };
        },
        getDOMstrings: function(){
            return DOMstrings;
        },
        addListItem: function(obj, type){
            var html, newHtml, element;
            if (type === 'inc'){
            element = DOMstrings.incomeContainer;
            html = '<div class="item clearfix" id="inc-%id%"> <div class="item__description"> %des% </div> <div class="right clearfix"> <div class="item__value">%value% ZŁ</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div> </div> </div>';
            }else if (type === 'exp'){
            element = DOMstrings.expensesContainer;
            html = '<div class="item clearfix" id="exp-%id%"> <div class="item__description"> %des% </div> <div class="right clearfix"> <div class="item__value">%value% ZŁ</div> <div class="item__percentage">%perc%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div> </div> </div>';
            }
            newHtml = html.replace('%id%', obj.id);
            newHtml = html.replace('%value%', obj.value);
            newHtml = html.replace('%des%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            newHtml = newHtml.replace('%id%', obj.id);
            newHtml = newHtml.replace('%perc%', obj.percentage + '%');

            
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },
        deleteListItem: function(selectedID){
            var element = document.getElementById(selectedID);
            element.parentNode.removeChild(element);

        },
        showTotal: function(data){
            
            
            element = DOMstrings.totalInc;
            document.querySelector(element).textContent = data.totals['inc'] + ' Zł';
            element = DOMstrings.totalExp;
            document.querySelector(element).textContent = data.totals['exp'] + ' Zł';
            element = DOMstrings.totalBudget;
            document.querySelector(element).textContent = parseInt(data.totals['inc'] - data.totals['exp']) + ' ZŁ';

        },
        setMonth: function(){
            var html;
            var d = new Date();
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var n = month[d.getMonth()];
            element = DOMstrings.UIMonth;
            html = '<div class="budget__title">Available Budget in <span class="budget__title--month">%Month%</span>:</div>'
            newHtml = html.replace('%Month%', n);
            document.querySelector(element).insertAdjacentHTML('afterbegin', newHtml);
        },
        clearFields: function(){
            var fields, fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);

            var fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function(curr, id, arr){
                curr.value = "";
            });
            fieldsArr[0].focus();
        },
        showPercentages: function(data){
            element = DOMstrings.expPercentage;
            document.querySelector(element).textContent = data.percentage['exp'] + ' %';
            element = DOMstrings.incPercentage;
            document.querySelector(element).textContent = data.percentage['inc'] + ' %';
            data.allItems['exp'].forEach(function(el){
                el.percentage = Math.round(el.value/(data.totals['exp'] + data.totals['inc'])*100);
            });
        },
        showIndivPercentages: function(data){
            var fields = document.querySelectorAll('.item__percentage');
            var nodeListForEach = function(list,callback){
                for (var i = 0; i <list.length; i++){
                    callback(list[i], i);
                }
            };

            nodeListForEach(fields, function(curr, index){
                curr.textContent = data.allItems['exp'][index].percentage + '%';
            });
        }
    };

})();

var controller = (function(budgetCtrl, UIctrl) {

    var setupEventListeners = function() {
        var DOM = UIctrl.getDOMstrings();
        document.querySelector(DOM.inputButton).addEventListener('click', ctrlAddItem);
        document.addEventListener('keypress', function(event){
        if(event.keyCode === 13 || event.which === 13){
            ctrlAddItem();
        }
        });
        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);

    };

    budgetController.readData();
    budgetController.getData().allItems['inc'].forEach(element => {
        UIcontroller.addListItem(element, 'inc')
    });
    budgetController.getData().allItems['exp'].forEach(element => {
        UIcontroller.addListItem(element, 'exp')
    });
    UIctrl.showTotal(budgetController.getData());
    UIctrl.showPercentages(budgetController.getData());
    UIcontroller.setMonth();

    var ctrlAddItem = function(){
        var input = UIctrl.getinput();
        
        if (input.description !== "" && !isNaN(input.value) && input.value > 0){
            var newItem = budgetController.addItem(input.type, input.description, input.value);
            UIctrl.addListItem(newItem, input.type);
            budgetController.calculateTotals();
            budgetController.calculatePercentages();
            UIctrl.showTotal(budgetController.getData());
            UIctrl.clearFields();
            UIctrl.showPercentages(budgetController.getData());
            UIctrl.showIndivPercentages(budgetController.getData());
            budgetController.saveData();

        }

    };

    var ctrlDeleteItem = function(event){
        var itemID, spiltID, type, ID;

        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if (itemID){
            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);
            budgetController.deleteItem(type, ID);
            budgetController.calculateTotals();
            budgetController.calculatePercentages();
            UIctrl.deleteListItem(itemID);
            UIctrl.showTotal(budgetController.getData());
            UIctrl.showPercentages(budgetController.getData());
            UIctrl.showIndivPercentages(budgetController.getData());
            budgetController.saveData();


        }
    };

    return {
        init: function(){
            console.log('Application has started');
            setupEventListeners();
        }
    }

})(budgetController,UIcontroller);

controller.init();